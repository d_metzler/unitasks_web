class Project < ActiveRecord::Base
  has_many :tasks, :dependent => :delete_all
  validates :title, presence: true
end
