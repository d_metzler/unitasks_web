class Task < ActiveRecord::Base
  belongs_to :project
  belongs_to :user
  validates :description, presence: true
  validates :user, presence: true
end
